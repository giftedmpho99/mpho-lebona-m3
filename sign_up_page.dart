import 'package:flutter/material.dart';
import 'package:my_app/login_screen.dart';
import 'package:my_app/profile_page.dart';
import 'package:my_app/page_one.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.indigoAccent,
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const LoginScreen()),
              );
            }),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
            width: w,
            height: h * 0.3,
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //     image: AssetImage('assets/VR.png'),
            //     fit: BoxFit.cover,
            //   ),
            // ),
          ),
          Container(
            width: w,
            margin: EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Sign up',
                  style: TextStyle(
                    fontSize: 50,
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),

                SizedBox(height: 30),
                // Text field 1
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.email_outlined),
                    // focusedBorder: OutlineInputBorder(
                    //     borderSide: BorderSide(
                    //   color: Colors.white,
                    // ),

                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    hintText: 'Email',
                  ),
                ),
                SizedBox(height: 20),
                // Text field 2
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.password_outlined),
                    fillColor: Colors.grey.shade200,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    hintText: 'Password',
                    // suffixIcon: Icon(Icons.remove_red_eye_outlined)
                  ),
                ),
                SizedBox(height: 20),

                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.password_outlined),
                    fillColor: Colors.grey.shade200,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    hintText: 'Confirm Password',
                    // suffixIcon: Icon(Icons.remove_red_eye_outlined)
                  ),
                ),
                SizedBox(height: 10),

                Row(
                  children: [
                    Expanded(
                      child: Container(),
                    ),
                  ],
                ),
                SizedBox(height: 15),
                Center(
                  child: Container(
                    width: 300,
                    height: 55,
                    child: RaisedButton(
                      padding: EdgeInsets.all(20),
                      shape: StadiumBorder(),
                      child: const Text(
                        'Sign up',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                      color: Colors.indigoAccent,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PageOneHome(),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                SizedBox(height: 29),
                Center(
                  child: Text(
                    'By signing up, you agree to the Terms of Use and Privacy Policy',
                    style: TextStyle(fontSize: 11, color: Colors.grey),
                  ),
                ),

                SizedBox(height: 10),
                Center(
                  child: RichText(
                    text: TextSpan(
                        text: "Already a member? ",
                        style: TextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          color: Colors.grey,
                        ),
                        children: [
                          TextSpan(
                            text: "Sign in",
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.indigoAccent,
                            ),
                          ),
                        ]),
                  ),
                ),

                // Container(
              ],
            ),
          ),
        ],
      ),
    );
  }
}
