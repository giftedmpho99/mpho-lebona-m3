import 'package:flutter/material.dart';
import 'package:my_app/login_screen.dart';
import 'package:my_app/sign_up_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner :false,
      title: 'Hula',
      theme: ThemeData(
        // primarySwatch: Colors.indigoAccent,
        primaryColor: Colors.indigoAccent,
      ),
      home: LoginScreen(),
    );
  }
}
