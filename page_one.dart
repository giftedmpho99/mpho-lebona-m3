import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:my_app/login_screen.dart';
import 'package:my_app/main.dart';
import 'package:my_app/profile_page.dart';

class PageOneHome extends StatelessWidget {
  const PageOneHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Center(
                  // child: Text(
                  //   "Mpho Lebona 🌻",
                  //   style: TextStyle(
                  //     fontSize: 32,
                  //     color: Colors.white
                  //   ),
                  // ),
                  ),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/VR.png'), fit: BoxFit.cover),
              ),
            ),
            ListTile(
              hoverColor: Colors.indigoAccent,
              leading: Icon(Icons.person_outline),
              title: Text('My Profile'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfilePage()));
              },
            ),
            ListTile(
              hoverColor: Colors.indigoAccent,
              leading: Icon(Icons.inbox_outlined),
              title: Text('Inbox'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const PageOneHome()));
              },
            ),
            ListTile(
              hoverColor: Colors.indigoAccent,
              leading: Icon(Icons.notifications_outlined),
              title: Text('Notifications'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const PageOneHome()));
              },
            ),
            ListTile(
              hoverColor: Colors.indigoAccent,
              leading: Icon(Icons.shield_outlined),
              title: Text('Security'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const PageOneHome()));
              },
            ),
            ListTile(
              hoverColor: Colors.indigoAccent,
              leading: Icon(Icons.search),
              title: Text('Search'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const PageOneHome()));
              },
            ),
            ListTile(
              hoverColor: Colors.indigoAccent,
              leading: Icon(Icons.help_center_outlined),
              title: Text('Help Center'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const PageOneHome()));
              },
            ),
            ListTile(
              hoverColor: Colors.indigoAccent,
              leading: Icon(Icons.logout),
              title: Text('Logout'),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginScreen()));
              },
            ),
          ],
        ),
      ), // Populate the Drawer in the next step.

      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              child: Text('Welcome to your App home Screen'),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => ProfilePage()));
          // Add your onPressed code here!
        },
        backgroundColor: Colors.indigoAccent,
        child: const Icon(Icons.person_outline),
      ),
    );
  }
}
